{
  inputs = {
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, fenix, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        fenixPkgs = (fenix.packages.${system}.stable);
        rustPlatform = (pkgs.makeRustPlatform {
          inherit (fenixPkgs) cargo rustc;
        });
      in
      {
        defaultPackage = rustPlatform.buildRustPackage {
          pname = "server-update-util";
          version = "0.0.0";
          src = ./.;
          cargoHash = "";
          nativeBuildInputs = with pkgs; [ openssl.dev pkg-config ];
        };

        devShell = pkgs.mkShell
          {
            buildInputs = with pkgs; [
              openssl.dev
              pkg-config
              fenixPkgs.toolchain
            ];
          };
      });
}
