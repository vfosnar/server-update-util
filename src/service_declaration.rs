use std::collections::HashMap;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct ServiceDeclaration {
    pub image: Option<Image>,
    pub git: Option<Git>,

    pub semver: Option<Semver>,

    pub services: Option<HashMap<String, SubService>>,
}

#[derive(Debug, Deserialize)]
pub struct Image {
    pub remote: String,
}

#[derive(Debug, Deserialize)]
pub struct Git {
    pub remote: String,
}

#[derive(Debug, Deserialize)]
pub struct Semver {
    pub extractor: Option<String>,
    pub restriction: String,
}

#[derive(Debug, Deserialize)]
pub struct SubService {
    pub r#static: Option<SubServiceStatic>,
    pub replace: Option<SubServiceReplace>,
}

#[derive(Debug, Deserialize)]
pub struct SubServiceStatic {
    pub value: String,
}

#[derive(Debug, Deserialize)]
pub struct SubServiceReplace {
    pub capture: String,
    pub generate: String,
}
