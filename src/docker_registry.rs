use std::collections::HashMap;

use http_auth::ChallengeParser;
use reqwest::StatusCode;
use serde::Deserialize;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Could not make request: {0}")]
    Network(String),
    #[error("Invalid response from the server: {0}")]
    InvalidResponse(#[from] InvalidResponseError),
    #[error("Unknown authentication method")]
    UnknownAuthenticationMethod,
    #[error("Request is not clonable")]
    RequestNotClonable,
    #[error("Could not construct a request")]
    CouldNotConstructRequest,
}

#[derive(Debug, Error)]
pub enum InvalidResponseError {
    #[error("Response is missing the www-authenticate header")]
    MissingWwwAuthenticateHeader,
    #[error("Header is not a valid string")]
    InvalidHeaderEncoding,
    #[error("Realm is missing from Bearer authentication")]
    RealmMissingFromBearerAuthentication,
    #[error("Invalid status code from realm endpoint: {0}")]
    InvalidStatusCodeFromRealmEndpoint(u16),
    #[error("Invalid body from realm endpoint")]
    InvalidJsonBody,
    #[error("Invalid value for header")]
    InvalidHeaderValue,
}

#[derive(Debug, Clone)]
pub struct Client {
    http_client: reqwest::Client,
    registry: String,
    registry_hostname: String,
}

impl Client {
    #[must_use]
    pub fn new(registry: &str) -> Self {
        let http_client = reqwest::Client::new();

        let registry_hostname = Self::map_registry_to_hostname(registry);

        Self {
            http_client,
            registry: registry.to_string(),
            registry_hostname,
        }
    }

    #[must_use]
    pub fn registry(&self) -> &str {
        &self.registry
    }

    #[must_use]
    pub fn registry_hostname(&self) -> &str {
        &self.registry_hostname
    }

    #[must_use]
    pub fn map_registry_to_hostname(registry: &str) -> String {
        match registry {
            "docker.io" => "registry-1.docker.io".to_string(),
            _ => registry.to_string(),
        }
    }

    pub async fn execute_authenticated(
        &self,
        request: reqwest::Request,
    ) -> Result<reqwest::Response, Error> {
        let response = {
            let request = request.try_clone().ok_or(Error::RequestNotClonable)?;
            self.http_client
                .execute(request)
                .await
                .map_err(|e| Error::Network(e.to_string()))?
        };

        if response.status() != StatusCode::UNAUTHORIZED {
            return Ok(response);
        }

        let www_authenticate = response
            .headers()
            .get("www-authenticate")
            .ok_or(Error::InvalidResponse(
                InvalidResponseError::MissingWwwAuthenticateHeader,
            ))?
            .to_str()
            .map_err(|_| Error::InvalidResponse(InvalidResponseError::InvalidHeaderEncoding))?;

        let params = ChallengeParser::new(www_authenticate)
            .filter_map(Result::ok)
            .find(|v| v.scheme.eq_ignore_ascii_case("bearer"))
            .ok_or(Error::UnknownAuthenticationMethod)?
            .params;

        let realm = params
            .iter()
            .find(|(key, _)| key.eq_ignore_ascii_case("realm"))
            .ok_or(Error::InvalidResponse(
                InvalidResponseError::RealmMissingFromBearerAuthentication,
            ))?
            .1
            .to_unescaped();

        let params_without_realm = params
            .iter()
            .filter(|(key, _)| !key.eq_ignore_ascii_case("realm"))
            .map(|(key, value)| ((*key).to_string(), value.to_unescaped()))
            .collect::<HashMap<String, String>>();

        let bearer_token_response = self
            .http_client
            .get(realm)
            .query(&params_without_realm)
            .send()
            .await
            .map_err(|e| Error::Network(e.to_string()))?
            .error_for_status()
            .map_err(|e| {
                Error::InvalidResponse(InvalidResponseError::InvalidStatusCodeFromRealmEndpoint(
                    // unwrap: https://docs.rs/reqwest/latest/reqwest/struct.Response.html#method.error_for_status
                    #[allow(clippy::unwrap_used)]
                    e.status().unwrap().as_u16(),
                ))
            })?
            .json::<BearerTokenResponse>()
            .await
            .map_err(|_| Error::InvalidResponse(InvalidResponseError::InvalidJsonBody))?;

        let response = {
            let mut request = request.try_clone().ok_or(Error::RequestNotClonable)?;
            request.headers_mut().append(
                "Authorization",
                format!("Bearer {}", bearer_token_response.token)
                    .try_into()
                    .map_err(|_| {
                        Error::InvalidResponse(InvalidResponseError::InvalidHeaderValue)
                    })?,
            );
            self.http_client
                .execute(request)
                .await
                .map_err(|e| Error::Network(e.to_string()))?
        };

        Ok(response)
    }

    pub async fn tags(&self, name: &str) -> Result<Vec<String>, Error> {
        let mut next_url = Some(format!(
            "https://{hostname}/v2/{name}/tags/list",
            hostname = self.registry_hostname
        ));
        let mut tags = vec![];
        while let Some(url) = next_url {
            let request = self
                .http_client
                .get(url)
                .build()
                .map_err(|_| Error::CouldNotConstructRequest)?;

            let response = self.execute_authenticated(request).await?;

            // save for later because .json() consumes the response
            let link_header_option = response.headers().get("link").cloned();

            let new_tags = response
                .json::<TagsResponse>()
                .await
                .map_err(|_| Error::InvalidResponse(InvalidResponseError::InvalidJsonBody))?
                .tags;

            // will break on an empty array
            let Some(last_new_tag) = new_tags.first() else {
                break;
            };

            // if the paging method used was not supported and returned already existing data
            if tags.contains(last_new_tag) {
                break;
            }

            next_url = Some(if let Some(link_header) = link_header_option {
                let link_header = link_header.to_str().map_err(|_| {
                    Error::InvalidResponse(InvalidResponseError::InvalidHeaderEncoding)
                })?;

                self.next_url_from_link_header(link_header)?
            } else {
                self.next_url_from_last_tag(name, last_new_tag)
            });

            tags.extend(new_tags.iter().cloned());
        }

        Ok(tags)
    }

    fn next_url_from_link_header(&self, link_header: &str) -> Result<String, Error> {
        let path = link_header
            .strip_prefix('<')
            .ok_or(Error::InvalidResponse(
                InvalidResponseError::InvalidHeaderValue,
            ))?
            .strip_suffix(">; rel=\"next\"")
            .ok_or(Error::InvalidResponse(
                InvalidResponseError::InvalidHeaderValue,
            ))?;

        Ok(format!(
            "https://{hostname}{path}",
            hostname = self.registry_hostname
        ))
    }

    fn next_url_from_last_tag(&self, name: &str, last_tag: &str) -> String {
        format!(
            "https://{hostname}/v2/{name}/tags/list?last={last_tag}",
            hostname = self.registry_hostname
        )
    }
}

#[derive(Debug, Deserialize)]
struct BearerTokenResponse {
    token: String,
}

#[derive(Debug, Deserialize)]
struct TagsResponse {
    tags: Vec<String>,
}
