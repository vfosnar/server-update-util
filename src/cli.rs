use std::{collections::HashMap, fs, str::FromStr};

use crate::{
    candidate_restrictions::SemverRestriction,
    config::Config,
    docker_registry,
    dotenvy_formatter::format_file,
    service_declaration::{self, ServiceDeclaration},
};

use clap::Parser;
use eyre::{bail, eyre, Context, Result};
use git2::{Direction, Remote};
use regex::Regex;

pub async fn main() -> Result<()> {
    let args = Config::parse();

    let mut env = Vec::new();

    let constants_content = fs::read_to_string(args.constants_config)?;
    let constants: HashMap<String, String> = toml::from_str(&constants_content)?;

    for (key, value) in constants {
        env.push((key, value));
    }

    let services_content = fs::read_to_string(args.services_config)?;
    let mut services = toml::from_str::<HashMap<String, ServiceDeclaration>>(&services_content)?
        .into_iter()
        .collect::<Vec<(String, ServiceDeclaration)>>();
    services.sort_by(|a, b| a.0.cmp(&b.0));

    // TODO use map instead
    for (name, service) in services {
        let service_env = handle_service(&name, &service)
            .await
            .with_context(|| format!("updating service '{name}' failed"))?;
        env.extend(service_env);
    }

    let env_content = format_file(env)?;
    fs::write(args.env_output, env_content)?;

    Ok(())
}

async fn handle_service(
    name: &str,
    declaration: &service_declaration::ServiceDeclaration,
) -> Result<Vec<(String, String)>> {
    println!("[{name}]");
    println!("- fetching");

    let version_to_id_map = if let Some(image) = &declaration.image {
        fetch_image_versions(image).await?
    } else if let Some(git) = &declaration.git {
        fetch_git_versions(git)?
    } else {
        bail!("either image or git entry must exist");
    };

    let Some(semver) = &declaration.semver else {
        bail!("semver entry must exist");
    };

    let restriction = SemverRestriction::from_str(&semver.restriction)?;
    let candidates = restriction.filter_versions(
        &version_to_id_map.keys().cloned().collect::<Vec<_>>(),
        semver.extractor.as_deref(),
    )?;

    if let Some(manual_version) = candidates.manual {
        let new_restriction = manual_version.0.new_restriction;
        if new_restriction != restriction {
            println!("NEW VERSION AVAILABLE: {new_restriction}");
        }
    }

    let Some(automatic_version) = candidates.automatic else {
        bail!("no version found for an automatic update");
    };
    let mut env = Vec::new();
    {
        let key = format!("VERSION_{}", name.to_uppercase());
        env.push((key, automatic_version.clone()));
    }

    if let Some(subservices) = &declaration.services {
        // TODO use map instead
        for (ss_name, ss_declaration) in subservices {
            let subservice_env =
                handle_subservice(name, ss_name, ss_declaration, &automatic_version)
                    .with_context(|| format!("updating subservice '{ss_name}' failed"))?;
            env.push(subservice_env);
        }
    }

    Ok(env)
}

fn handle_subservice(
    service_name: &str,
    name: &str,
    declaration: &service_declaration::SubService,
    automatic_version: &str,
) -> Result<(String, String)> {
    let value = if let Some(r#static) = &declaration.r#static {
        r#static.value.clone()
    } else if let Some(replace) = &declaration.replace {
        let regex = Regex::new(&replace.capture).context("invalid regex for capture")?;
        regex
            .replace_all(automatic_version, &replace.generate)
            .to_string()
    } else {
        bail!("either static or replace table must exist");
    };

    let key = format!(
        "VERSION_{}_{}",
        service_name.to_uppercase(),
        name.to_uppercase()
    );
    Ok((key, value.to_string()))
}

async fn fetch_image_versions(
    image: &service_declaration::Image,
) -> Result<HashMap<String, String>> {
    let (registry, namespace) = image
        .remote
        .split_once('/')
        .ok_or(eyre!("not a valid image reference"))?;

    let client = docker_registry::Client::new(registry);
    let versions = client
        .tags(namespace)
        .await?
        .into_iter()
        .map(|tag| (tag.clone(), tag))
        .collect();
    Ok(versions)
}

fn fetch_git_versions(git: &service_declaration::Git) -> Result<HashMap<String, String>> {
    let mut remote = Remote::create_detached(git.remote.clone())?;
    remote.connect(Direction::Fetch)?;

    let references = remote.list()?;
    let versions = references
        .iter()
        .filter_map(|reference| {
            let name = reference.name().strip_prefix("refs/tags/")?;
            if name.ends_with("^{}") {
                return None;
            }
            Some((name.to_owned(), reference.oid().to_string()))
        })
        .collect();
    Ok(versions)
}
