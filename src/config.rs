use std::path::PathBuf;

use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Config {
    #[clap(long, default_value = "constants.toml")]
    pub constants_config: PathBuf,
    #[clap(long, default_value = "services.toml")]
    pub services_config: PathBuf,
    #[clap(long, default_value = ".env")]
    pub env_output: PathBuf,
}
