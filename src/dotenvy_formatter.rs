use lazy_static::lazy_static;
use regex::Regex;
use thiserror::Error;

lazy_static! {
    static ref VARIABLE_NAME_REGEX: Regex = Regex::new("^[a-zA-Z_]+[a-zA-Z0-9_]*$").unwrap();
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("Invalid environment variable name: '{0}'")]
    InvalidEnvironmentVariableName(String),
}

pub fn format_file(env: Vec<(String, String)>) -> Result<String, Error> {
    let mut content = env
        .into_iter()
        .map(|(name, value)| format_line(&name, &value))
        .collect::<Result<Vec<_>, _>>()?
        .join("\n");

    // append last newline
    content.push('\n');

    Ok(content)
}

pub fn format_line(name: &str, value: &str) -> Result<String, Error> {
    if !check_variable_name(name) {
        return Err(Error::InvalidEnvironmentVariableName(name.to_string()));
    }

    Ok(format!("{name} = \"{}\"", escape_variable_value(value)))
}

pub fn check_variable_name(variable_name: &str) -> bool {
    VARIABLE_NAME_REGEX.is_match(variable_name)
}

pub fn escape_variable_value(variable_value: &str) -> String {
    variable_value
        .chars()
        .map(escape_variable_value_char)
        .collect::<String>()
}

pub fn escape_variable_value_char(char: char) -> String {
    match char {
        '\n' => "\\n".to_string(),
        '\r' => "\\r".to_string(),
        '\t' => "\\t".to_string(),
        '\u{c}' => "\\f".to_string(),
        '\u{8}' => "\\b".to_string(),
        '"' => "\\\"".to_string(),
        '\'' => "\\'".to_string(),
        '\\' => "\\".to_string(),
        char => char.to_string(),
    }
}
