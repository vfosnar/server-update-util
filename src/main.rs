mod candidate_restrictions;
mod cli;
mod config;
pub mod docker_registry;
mod dotenvy_formatter;
mod service_declaration;

#[tokio::main]
async fn main() {
    if let Err(e) = cli::main().await {
        eprintln!("{e:?}");
    }
}
