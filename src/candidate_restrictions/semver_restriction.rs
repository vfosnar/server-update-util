use std::{cmp::Ordering, fmt::Display, str::FromStr};

use eyre::{bail, ContextCompat, Error, Result};
use lazy_static::lazy_static;
use regex::Regex;

use super::CandidateRestrictions;

const DEFAULT_SEMVER_EXTRACTOR_REGEX: &str = r"^v?(\d.*?)$";

lazy_static! {
    static ref VERSION_RESTRICTION_REGEX: Regex =
        Regex::new(r"^(\d+)(?:\.(\d+)(?:\.(\d+))?)?$").unwrap();
}

#[derive(Debug, PartialEq, Eq)]
pub enum SemverRestriction {
    Major(u64),
    Minor(u64, u64),
    Patch(u64, u64, u64),
}

impl FromStr for SemverRestriction {
    type Err = Error;

    fn from_str(text: &str) -> Result<Self> {
        let Some(captures) = VERSION_RESTRICTION_REGEX.captures(text) else {
            bail!("not a valid restriction");
        };

        // major is always captured
        let Ok(major) = captures.get(1).unwrap().as_str().parse() else {
            bail!("major must be a number");
        };

        // capture minor or return as Major
        let Some(minor) = captures.get(2) else {
            return Ok(Self::Major(major));
        };
        let Ok(minor) = minor.as_str().parse() else {
            bail!("minor must be a number");
        };

        // capture patch or return as Minor
        let Some(patch) = captures.get(3) else {
            return Ok(Self::Minor(major, minor));
        };
        let Ok(patch) = patch.as_str().parse() else {
            bail!("patch must be a number");
        };

        Ok(Self::Patch(major, minor, patch))
    }
}

impl Display for SemverRestriction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SemverRestriction::Major(major) => write!(f, "{major}"),
            SemverRestriction::Minor(major, minor) => write!(f, "{major}.{minor}"),
            SemverRestriction::Patch(major, minor, patch) => write!(f, "{major}.{minor}.{patch}"),
        }
    }
}

impl SemverRestriction {
    pub fn filter_versions(
        &self,
        versions: &[String],
        extractor: Option<&str>,
    ) -> Result<CandidateRestrictions<NewSemverRestriction>> {
        println!("- filtering");

        let stable_versions = versions
            .iter()
            .map(|str_version| {
                let Some(semver_text) = extract_semver(str_version, extractor)? else {
                    return Ok(None);
                };
                let Ok(version) = semver::Version::parse(semver_text) else {
                    return Ok(None);
                };
                Ok(Some((version, str_version.clone())))
            })
            .filter_map(Result::transpose)
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .filter(|(version, _)| version.pre.is_empty())
            .collect::<Vec<_>>();

        let candidate_automatic = self.find_highest_version(&stable_versions);

        let candidate_manual = stable_versions.iter().max_by(compare_first_in_tuple);

        Ok(CandidateRestrictions {
            automatic: candidate_automatic.map(|(_, version_str)| version_str.clone()),
            manual: candidate_manual.map(|(version, version_str)| {
                (
                    NewSemverRestriction {
                        new_restriction: match self {
                            SemverRestriction::Major(_) => SemverRestriction::Major(version.major),
                            SemverRestriction::Minor(_, _) => {
                                SemverRestriction::Minor(version.major, version.minor)
                            }
                            SemverRestriction::Patch(_, _, _) => SemverRestriction::Patch(
                                version.major,
                                version.minor,
                                version.patch,
                            ),
                        },
                    },
                    version_str.clone(),
                )
            }),
        })
    }

    fn find_highest_version<'a>(
        &self,
        versions: &'a [(semver::Version, String)],
    ) -> Option<&'a (semver::Version, String)> {
        match self {
            SemverRestriction::Major(major) => versions
                .iter()
                .filter(|(version, _)| version.major == *major)
                .max_by(compare_first_in_tuple),
            SemverRestriction::Minor(major, minor) => versions
                .iter()
                .filter(|(version, _)| version.major == *major && version.minor == *minor)
                .max_by(compare_first_in_tuple),
            SemverRestriction::Patch(major, minor, patch) => versions
                .iter()
                .filter(|(version, _)| {
                    version.major == *major && version.minor == *minor && version.patch == *patch
                })
                .max_by(compare_first_in_tuple),
        }
    }
}

fn extract_semver<'a>(
    string_version: &'a str,
    semver_extractor: Option<&str>,
) -> Result<Option<&'a str>> {
    let re_str = semver_extractor.unwrap_or(DEFAULT_SEMVER_EXTRACTOR_REGEX);
    let re = Regex::new(re_str)?;

    let Some(caps) = re.captures(string_version) else {
        return Ok(None);
    };
    let semver_text = caps
        .get(1)
        .with_context(|| format!("extractor '{re_str}' is missing a capture group"))?
        .as_str();
    Ok(Some(semver_text))
}

fn compare_first_in_tuple<A: Eq + Ord, B>(t1: &&(A, B), t2: &&(A, B)) -> Ordering {
    t1.0.cmp(&t2.0)
}

pub struct NewSemverRestriction {
    pub new_restriction: SemverRestriction,
}
