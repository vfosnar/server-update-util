mod semver_restriction;

pub use semver_restriction::SemverRestriction;

#[derive(Debug)]
pub struct CandidateRestrictions<M> {
    pub automatic: Option<String>,
    pub manual: Option<(M, String)>,
}
