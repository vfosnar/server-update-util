FROM docker.io/library/rust:1-slim-bullseye AS builder
RUN apt-get update && \
    apt-get install -y ca-certificates libssl-dev openssl pkg-config
WORKDIR /usr/src/server-update-util
COPY . .
RUN cargo install --path .
FROM docker.io/library/debian:bullseye-slim
RUN apt-get update && \
    apt-get install -y ca-certificates openssl pkg-config && \
    rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/server-update-util /usr/local/bin/server-update-util
ENTRYPOINT ["server-update-util"]
